const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        /*backgroundColor: theme => ({
            ...theme('colors'),
            'blue': '#00253F',
            'blue-600': '#00487A',
            'border-blue': '#00253F',
        })*/
    },
    variants: {
        extend: {},
    },
    plugins: [],
}