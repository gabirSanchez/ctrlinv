import React, { useState } from 'react';

import { setLogin } from '../Endpoint';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import logo from '../../assets/img/logo.jpg';

const Login = ({ setToken }) => {

    const [usuario, setUser] = useState('');
    const [contrasenia, setPassword] = useState('');
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    const changeUser = (event) => {
        setUser(event.target.value);
    }

    const changePassword = (event) => {
        setPassword(event.target.value);
    }

    const handleLogin = async (event) => {
        event.preventDefault()
        if (usuario === "" || contrasenia === "") {
            Swal.fire({
                title: 'Información',
                text: 'Todos los campos son requeridos',
                icon: 'info',
                confirmButtonText: 'Ok',
            })
            return
        }

        setError(null);
        setLoading(true);

        const result = await setLogin({ usuario, contrasenia });
        console.log(result);
        if (result.status === "error") {
            Swal.fire({
                title: 'Información',
                text: result.message,
                icon: 'error',
                showConfirmButton: true,
                timer: 2000,
                timerProgressBar: true,
            })
            setLoading(false);
        } else {
            console.log('Ingresando al Sistemas',);
            setToken(result.data)
            setTimeout(() => {
                window.location.reload()
            }, 100);
        }
    }

    return (
        <section className="App h-screen w-full flex flex-col justify-center items-center bg-blue-app px-2">
            <div className="  h-200 max-w-600">
                <img src={logo} alt="Logo" className="  object-contain" />
            </div>

            <div className=" w-400" >

                <form action="" crossOrigin="*" className=" border-solid border border-white bg-transparent rounded px-4 py-4 mt-4">

                    <div className="flex items-center px-2 pt-4 border-white border-b ">
                        <i className='fa fa-user fa-20 text-green-app'></i>
                        <input type="text" name="usuario" id="usuario" className=" bg-transparent appearance-none w-full py-2 px-3 text-white leading-tight focus:outline-none " placeholder="Ingrese Usuario" autoComplete="off" value={usuario} onChange={changeUser} />
                    </div>

                    <div className="flex items-center px-2 pt-4 border-white border-b ">
                        <i className='fa fa-lock fa-20 text-green-app'></i>
                        <input type="password" name="contrasenia" id="contrasenia" className="bg-transparent appearance-none w-full py-2 px-3 text-white leading-tight focus:outline-none" autoComplete="off" placeholder="Ingrese Contraseña" value={contrasenia} onChange={changePassword} />
                    </div>

                    <div className="mt-4 w-full text-right ">
                        {loading ? <button className="btn_login focus:outline-none " type="button" disabled > <i className="fa fa-spinner fa-pulse fa-20 fa-fw"></i> </button> : <button className="btn_login focus:outline-none " type="button" onClick={handleLogin} > Ingresar </button>}
                    </div>

                    {error && <>
                        <div className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                            <strong className="font-bold">Error:</strong>
                            <span className="block sm:inline">{error}</span>
                        </div>
                    </>}
                </form>
            </div>


        </section>
    );
};

export default Login;