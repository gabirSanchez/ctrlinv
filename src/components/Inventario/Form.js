import React, { useState, useEffect } from 'react';

import Select from 'react-select';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { setVehicle } from '../Endpoint';

const Form = ({ token, setVehicleCreated }) => {

    const [type, setType] = useState({})
    const [listTypes, setListTypes] = useState([
        {value: 1, label: "Motocicletas"},
        {value: 2, label: "Sedan"}
    ])
    const [numRims, setNumRims] = useState(0)
    const [potency, setPotency] = useState("")

    const changeType = selected => {
        setType(selected);
    };

    const _setNumRims = (event) => {
        setNumRims(event.target.value)
        if (type.value === 1 && event.target.value > 2) {

            Swal.fire({
                title: 'Está usted seguro?',
                text: `por defecto las ${type.label}, solo tiene 2 Llantas, desea continuar?`,
                icon: 'warning',
                confirmButtonColor: '#003B3F',
                confirmButtonText: ' <i class="fa fa-check  "></i> Si, Continuar',
                denyButtonText: 'No',
                denyButtonColor: "gainsboro",
                showDenyButton: true,
                showCloseButton: true,
                allowOutsideClick: false,
            }).then(async (result) => {
                if (result.isDenied || result.isDismissed) {
                    setNumRims(0);
                } 

            })

        } 
    }
    const _setPotency = (event) => {
        setPotency(event.target.value);
    }

    const onSubmit = async () => {

        if (type.value === undefined || numRims === "" || potency === "") {
            Swal.fire({
                title: 'Información',
                html: 'Los campos marcados con (<span class="text-red-800 font-bold">*</span>) son requeridos',
                icon: 'info',
                confirmButtonText: 'Ok',
            })
            return
        } else if ( numRims === 0) {
            Swal.fire({
                title: 'Información',
                html: 'El número de llantas no puede ser 0',
                icon: 'info',
                confirmButtonText: 'Ok',
            })
            return
        }
        Swal.fire({
            title: 'Creando Vehiculo',
            html: 'Por favor espere un momento.',
            didOpen: () => {
                Swal.showLoading()
            }
        })

        // Create an object of formData 
        const _vehicle = {
            'tipo': type.label,
            'num_llantas': numRims,
            'potencia': potency,            
        }
        
        const result = await setVehicle(_vehicle, token)
        let insertIdT = result.insertId

        if (result.status === "error") {
            Swal.fire({
                title: 'Advertencia',
                html: 'Se ha presentado un error inesperado al guardar tu registro. <br /><br />Por favor, Intentalo de nuevo.<br />Si contunia el error contácta a aún Administrador.<br />&nbsp;',
                icon: 'error',
                confirmButtonText: 'Ok',
            })
            return
        } else {
            limpiarForm()
            setVehicleCreated(true)
            Swal.fire({
                title: `Vehiculo Creado!`,
                text: ``,
                icon: "success",
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
            })

        }
    }

    const limpiarForm = () => {
        setNumRims("")
        setPotency("")
        setType({})
    }

    return (
        <div className=" bg-white shadow-lg rounded-lg">
            <div className="titulo">Crear Vehiculo</div>
            <form className="w-full py-2 px-5">
                <div className="md:flex md:items-center mb-2">
                    <div className="md:w-1/3">
                        <label className="label" htmlFor="asunto">Asunto <span className="text-red-800 font-bold">*</span></label>
                    </div>
                    <div className="md:w-2/3">
                        <Select
                            value={type}
                            onChange={changeType}
                            options={listTypes}
                            placeholder={"Tipos"}
                            className={"  "}
                        />
                    </div>
                </div>
               
                <div className="md:flex md:items-center mb-2">
                    <div className="md:w-1/3">
                        <label className="label" htmlFor="num_llantas">Num Llantas <span className="text-red-800 font-bold">*</span></label>
                    </div>
                    <div className="md:w-2/3 ">
                       <input type="number" name="num_llantas" id="num_llantas" className=" input appearance-none focus:border-blue-app " placeholder="Num de Llantas" autoComplete="off" value={numRims} onChange={_setNumRims} />
                    </div>
                </div>
                <div className="md:flex md:items-center mb-2">
                    <div className="md:w-1/3">
                        <label className="label" htmlFor="potencia">Potencia <span className="text-red-800 font-bold">*</span></label>
                    </div>
                    <div className="md:w-2/3 ">
                        <input type="text" name="potencia" id="potencia" className=" input appearance-none focus:border-blue-app " placeholder="Potencia del motor" autoComplete="off" value={potency} onChange={_setPotency} />
                    </div>
                </div>

                <div className="md:flex md:items-center">
                    <div className="md:w-1/3"></div>
                    <div className="md:w-2/3 text-right">
                        <button className="btn-primary" type="button" onClick={onSubmit}>Crear</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default Form;