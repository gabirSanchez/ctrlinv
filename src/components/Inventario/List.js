import React, { useState, useEffect } from 'react';
import { getVehiculos } from '../Endpoint';
import DataTable from 'react-data-table-component';

const List = ({ token, vehicleCreated, setVehicleCreated }) => {

    const [datos, setDatos] = useState([])
    const [cargando, setCargando] = useState(false)
    const [total, setTotal] = useState(0)
    const [xpagina, setXPagina] = useState(10)

    const obtenerRegistros = async pagina => {
        setCargando(true)
        const result = await getVehiculos(token, pagina, xpagina)
        if (result.status === "error") {
            setDatos([])
            setTotal(0)
        } else {
            setDatos(result.data)
            setTotal(result.total)
        }
        setCargando(false)
    }

    const cambioCantidadItemPorPagina = async (nuevaXPagina) => {
        setCargando(true);
        const result = await getVehiculos(token, 1, nuevaXPagina)
        if (result.status === "error") {
            setDatos([])
            setTotal(0)
        } else {
            setDatos(result.data)
            setTotal(result.total)
        }
        setXPagina(nuevaXPagina);
        setCargando(false);
    };

    const cambioPagina = pagina => {
        obtenerRegistros(pagina);
    };

    const filtrando = async () => {
        setCargando(true)
        const result = await getVehiculos(token, 1, xpagina)

        if (result.status === "error") {
            setDatos([])
            setTotal(0)
        } else {
            console.log(result)
            setDatos(result.data)
            setTotal(result.total)
        }
        setCargando(false)
    }

    useEffect(() => {
        if ( vehicleCreated ) {
            filtrando()
        }
        setVehicleCreated(false)
    }, [vehicleCreated]);


    const columnas = [
        {
            name: "ID",
            selector: row => row.id,
            sortable: true,
            grow: .5,
        }, {
            name: "Tipo",
            selector: row => row.tipo,
            sortable: true,
            grow: 3,
            wrap: true,
        }, {
            name: "Núm LLantas",
            selector: row => row.num_llantas,
            sortable: true,
            center: true,
            wrap: true
        }, {
            name: "Potencia",
            selector: row => row.potencia,
            sortable: true,
            wrap: true,
        }, {
            name: "Usuario",
            selector: row => row.nombres + ' ' + row.apellidos,
            sortable: true,
            wrap: true,
        }
    ]
    const paginacionOptions = {
        rowsPerPageText: 'Filas por página',
        rangeSeparatorText: 'de',
        selectAllRowsItem: true,
        selectAllRowsItemText: 'Todos',
    }

    return (
        <div className="h-full">
            <div className="h-full bg-white shadow-lg rounded-lg relative" >
                <div className="titulo">Vehiculos </div>
               

                <DataTable
                    columns={columnas}
                    data={datos}
                    progressPending={cargando}
                    pagination
                    paginationServer
                    paginationTotalRows={total}
                    onChangePage={cambioPagina}
                    onChangeRowsPerPage={cambioCantidadItemPorPagina}
                    paginationComponentOptions={paginacionOptions}
                    fixedHeader
                    fixedHeaderScrollHeight="5000px"
                    striped
                    responsive
                    highlightOnHover
                    noHeader={true}
                    noDataComponent={<p className="subtitulo">No hay registros para mostrar</p>}
                />

            </div>

        </div>
    );
};

export default List;