import React, { useState, useEffect } from 'react';
import logo from '../../assets/img/logo.jpg';
import { useJwt } from "react-jwt";

import Form from './Form';
import List from './List';

const Inventario = ({ token }) => {

    const { decodedToken, isExpired } = useJwt(token);
    const [vehicleCreated, setVehicleCreated] = useState(true);
    //console.log( decodedToken, isExpired)

    return (
        <section className="App h-screen w-full flex flex-row justify-center items-center bg-blue-app px-2 text-blue-app">
            <article className="w-1/3 h-full flex flex-col justify-between_ ">
                <div className="md:w-80 my-2 self-center ">
                    <img src={logo} alt="Logo" className=" object-contain " />
                </div>
                <div className="pb-2">
                    <div className=" py-2 bg-transparent shadow-lg rounded-lg ">
                        <h2 className="text-white text-xl ">Hola, {decodedToken !== null ? <span>{decodedToken.nombres} {decodedToken.apellidos}</span> : null}</h2>
                    </div>
                    <Form token={token} setVehicleCreated={setVehicleCreated} />
                </div>
            </article>
 
            <article className="w-2/3 h-full py-1 ml-1  " >
                <List token={token}  vehicleCreated={vehicleCreated} setVehicleCreated={setVehicleCreated} />
            </article>
        </section>
    );
};

export default Inventario;