//import axios from "axios";
const URL = "http://192.168.100.91/api_ctrlinv/index.php/api/";
const API_KEY = '16fe3fb7030eb8dbf7e1f825b55b9bb7';

// ---------------------        GET         ---------------------------- //
let requestOptionsGet = {
    method: 'GET',
    headers: { 
        'Content-Type': 'application/json',
        'x-api-key': API_KEY,
        'Authorization': ''
    },
};

export const getVehiculos = async (token, page, per_page) => {

    requestOptionsGet.headers.Authorization = 'Bearer '+ token
    return await fetch(URL + 'vehiculo', requestOptionsGet)
        .then(resp => resp.json())
        .then(data => {
            return data
        })
        .catch(err => {
            return err
        })
}

// ---------------------        POST         ---------------------------- //
const requestOptionsPOST = {
    method: 'POST',
    headers: { 
        'Content-Type': 'application/json',
        'x-api-key': API_KEY,
        'Authorization': ''
    },
    body: '',
};

export const setLogin = async (params) => {
   
    requestOptionsPOST.body = JSON.stringify(params)
    console.log("LOGIN ", requestOptionsPOST)
    return await fetch(URL + 'auth/login', requestOptionsPOST)
        .then(resp => resp.json())
        .then(data => {
            return data
        })
        .catch(err => {
            return (err)

        })
}

export const setVehicle = async (params, token) => {

    requestOptionsPOST.body = JSON.stringify(params)
    requestOptionsPOST.headers.Authorization = 'Bearer '+ token
    return await fetch(URL + 'vehiculo', requestOptionsPOST)
        .then(resp => resp.json())
        .then(data => {
            return data
        })
        .catch(err => {
            return err
        })
}

/// --------------------------- PUT ----------------------//

// -------------------------  DELETE -------------------------- //
