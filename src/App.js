import '../node_modules/font-awesome/css/font-awesome.min.css'; 
import './assets/css/sweetalert2.css';

import Login from './components/Login/Login';
import Inventario from './components/Inventario/Inventario';


const App = () => {

  const setToken = ( user ) => {
    sessionStorage.setItem('tkn-ctrlInv', JSON.stringify(user) );
  }
  const getToken = () => {
    let token = sessionStorage.getItem('tkn-ctrlInv')
    return JSON.parse(token)
  }

  const token = getToken()

  if ( !token ) {
    return <Login setToken={setToken} />
  } else {
    return <Inventario token={token} />
  }
}

export default App;
